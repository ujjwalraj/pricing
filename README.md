@POST http://localhost:10002/pricing/v1/fetch_plans

{
	"merchantName":"SIMPLILEARN",
	"productId":"COURSE_A"
}

Response:

{
  "merchantName": "SIMPLILEARN",
  "products": [
    {
      "productId": "COURSE_A",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_1",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_1",
            "tenure": 9,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    }
  ]
}


2.@POST http://localhost:10002/pricing/v1/fetch_plans

{
	"merchantName":"SIMPLILEARN"
}

Response:
{
  "merchantName": "SIMPLILEARN",
  "products": [
    {
      "productId": "COURSE_A",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_1",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_1",
            "tenure": 9,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_B",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_1",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_1",
            "tenure": 9,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_C",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_2",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_2",
            "tenure": 9,
            "merchantSubventionPercentage": 3,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0.75,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_D",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_2",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_2",
            "tenure": 9,
            "merchantSubventionPercentage": 3,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0.75,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_E",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_2",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_2",
            "tenure": 9,
            "merchantSubventionPercentage": 3,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0.75,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_F",
      "downpaymentPercentage": 10,
      "pricing": {
        "planId": "Plan_3",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_3",
            "tenure": 9,
            "merchantSubventionPercentage": 8,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_G",
      "downpaymentPercentage": 20,
      "pricing": {
        "planId": "Plan_3",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_3",
            "tenure": 9,
            "merchantSubventionPercentage": 8,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    },
    {
      "productId": "COURSE_H",
      "downpaymentPercentage": 20,
      "pricing": {
        "planId": "Plan_3",
        "planTenureDetails": [
          {
            "planTenureId": "3_month_1",
            "tenure": 3,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "6_month_1",
            "tenure": 6,
            "merchantSubventionPercentage": 5,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "9_month_3",
            "tenure": 9,
            "merchantSubventionPercentage": 8,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 0,
            "downPaymentType": "product_specific"
          },
          {
            "planTenureId": "12_month_1",
            "tenure": 12,
            "merchantSubventionPercentage": 0,
            "merchantFeePercentage": 1,
            "customerProcFeePercentage": 2,
            "customerRoi": 1.5,
            "downPaymentType": "product_specific"
          }
        ]
      }
    }
  ]
}

