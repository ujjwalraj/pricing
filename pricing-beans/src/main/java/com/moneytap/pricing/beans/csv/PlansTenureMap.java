package com.moneytap.pricing.beans.csv;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"planTenureId","tenure", "merchantSubventionPercentage", "merchantFeePercentage",
	"customerProcFeePercentage", "customerRoi", "downpaymentType"})
public class PlansTenureMap {
	private String planTenureId;
	private Integer tenure;
	private String merchantSubventionPercentage;
	private String merchantFeePercentage;
	private String customerProcFeePercentage;
	private String customerRoi;
	private String downpaymentType;
}
