package com.moneytap.pricing.beans.csv;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"planId", "subPlanId3months", "subPlanId6months", "subPlanId9months", "subPlanId12months"})
public class Plan {
	private String planId;
	private String subPlanId3months;
	private String subPlanId6months;
	private String subPlanId9months;
	private String subPlanId12months;

}
