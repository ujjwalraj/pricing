package com.moneytap.pricing.beans.csv;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.moneytap.application.ChannelPartner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"merchant", "productId", "pricing", "downpayment_perc"})
public class MerchantProductDetails {
	private ChannelPartner merchant;
	private String productId;
	private String pricing;
	private String downpayment_perc;
}
