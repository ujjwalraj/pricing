package com.moneytap.pricing.api;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.moneytap.application.ChannelPartner;
import com.moneytap.common.utilities.JsonUtil;
import com.moneytap.pricing.api.config.MerchantPricingConfig;
import com.moneytap.pricing.beans.csv.MerchantProductDetails;
import com.moneytap.pricing.beans.csv.Plan;
import com.moneytap.pricing.beans.csv.PlansTenureMap;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MerchantProductPlanAggregatorService {
	@Autowired
	private MerchantPricingConfig merchantPricingConfig;
	@Autowired
	private ResourceLoader resourceLoader;
	private Map<ChannelPartner, List<MerchantProductDetails>> merchantProductMap = new HashMap<>();
	private Map<String, Plan> plansMap = new HashMap<>();
	private Map<String, PlansTenureMap> planTenureMap = new HashMap<>();

	@PostConstruct
	private void init() throws IOException {
		LocalDateTime start = LocalDateTime.now();
		loadMerchantProductMap();
		loadPlanstMap();
		loadPlanTenureMap();
		LocalDateTime end = LocalDateTime.now();
		log.debug("Loaded merchantProductMap: {} plansMap: {}  planTenureMap:{} in {} ms.",
				merchantProductMap.values().size(), plansMap.values().size(), planTenureMap.values().size(),
				ChronoUnit.MILLIS.between(start, end));
	}

	public List<MerchantProductDetails> getMerchantProductDetails(ChannelPartner cp) {
		return merchantProductMap.get(cp);

	}

	public Plan getPlanSummary(String planId) {
		return plansMap.get(planId);

	}

	public PlansTenureMap getPlanTenureDetails(String planTenureId) {
		return planTenureMap.get(planTenureId);

	}

	private void loadPlanTenureMap() throws IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(MerchantProductDetails.class).withColumnSeparator(',').withHeader();

		Resource res = resourceLoader.getResource(merchantPricingConfig.getMerchantPricingFile());
		InputStream fis = res.getInputStream();
		MappingIterator<MerchantProductDetails> it = mapper.readerFor(MerchantProductDetails.class).with(schema)
				.readValues(fis);
		while (it.hasNext()) {
			MerchantProductDetails mpd = it.next();
			if (merchantProductMap.get(mpd.getMerchant()) != null) {
				merchantProductMap.get(mpd.getMerchant()).add(mpd);
			} else {
				List<MerchantProductDetails> list = new ArrayList<>();
				list.add(mpd);
				merchantProductMap.put(mpd.getMerchant(), list);
			}
		}

	}

	private void loadPlanstMap() throws IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(Plan.class).withColumnSeparator(',').withHeader();

		Resource res = resourceLoader.getResource(merchantPricingConfig.getPlansFile());
		InputStream fis = res.getInputStream();
		MappingIterator<Plan> it = mapper.readerFor(Plan.class).with(schema).readValues(fis);
		while (it.hasNext()) {
			Plan plan = it.next();
			plansMap.put(plan.getPlanId(), plan);
		}

	}

	private void loadMerchantProductMap() throws IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(PlansTenureMap.class).withColumnSeparator(',').withHeader();

		Resource res = resourceLoader.getResource(merchantPricingConfig.getPlanTenureFile());
		InputStream fis = res.getInputStream();
		MappingIterator<PlansTenureMap> it = mapper.readerFor(PlansTenureMap.class).with(schema).readValues(fis);
		while (it.hasNext()) {
			PlansTenureMap ptd = it.next();
			planTenureMap.put(ptd.getPlanTenureId(), ptd);
		}

	}

	
	public static void main(String[] args) throws IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(PlansTenureMap.class).withColumnSeparator(',').withHeader();

		InputStream fis = new FileInputStream("/home/user/pricing/pricing-api/src/main/resources/plans_tenure_map.csv");

		/*
		 * Resource res =
		 * resourceLoader.getResource(merchantPricingConfig.getPlanTenureFile());
		 * InputStream fis = res.getInputStream();
		 */

		MappingIterator<PlansTenureMap> it = mapper.readerFor(PlansTenureMap.class).with(schema).readValues(fis);
		while (it.hasNext()) {
			PlansTenureMap ptd = it.next();
			System.out.println(JsonUtil.obj2str(ptd));
			// planTenureMap.put(ptd.getPlanTenureId(), ptd);
		}
	}
	 
}
