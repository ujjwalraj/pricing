package com.moneytap.pricing.api.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component("fullertonProperties")
@ConfigurationProperties("merchant")
public class MerchantPricingConfig {
	private String merchantPricingFile;
	private String plansFile;
	private String planTenureFile;
}
