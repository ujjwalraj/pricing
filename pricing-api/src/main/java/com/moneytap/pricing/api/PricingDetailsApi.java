package com.moneytap.pricing.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.moneytap.api.spec.AbstractApi;
import com.moneytap.common.utilities.EqualsHelper;
import com.moneytap.pricing.beans.csv.MerchantProductDetails;
import com.moneytap.pricing.beans.csv.Plan;
import com.moneytap.pricing.beans.csv.PlansTenureMap;
import com.moneytap.pricing.io.MerchantPricingRequest;
import com.moneytap.pricing.io.MerchantPricingResponse;
import com.moneytap.pricing.io.MerchantPricingResponse.PlantTenureDetails;
import com.moneytap.pricing.io.MerchantPricingResponse.PricingDetails;
import com.moneytap.pricing.io.MerchantPricingResponse.Product;
import com.mwyn.rest.exception.ValidationException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PricingDetailsApi extends AbstractApi<MerchantPricingRequest, MerchantPricingResponse> {
	private static String NA = "NA";
	@Autowired
	private MerchantProductPlanAggregatorService merchantAggregatorService;

	@Override
	public MerchantPricingResponse callApi(MerchantPricingRequest in) {
		List<MerchantProductDetails> mpdList = validateAndGetMerchantProductDetails(in);
		List<MerchantProductDetails> productList = validateAndGetProducts(in, mpdList);
		MerchantPricingResponse retVal = new MerchantPricingResponse();
		List<Product> listOfProduct= new ArrayList<>();
		retVal.setMerchantName(in.getMerchantName());
		retVal.setProducts(listOfProduct);
		for (MerchantProductDetails mpd : productList) {
			listOfProduct.add(getProduct(mpd));
		}
		return retVal;
	}

	private Product getProduct(MerchantProductDetails mpd) {
		Product product = new Product();
		product.setProductId(mpd.getProductId());
		if (isValidNumeric(mpd.getDownpayment_perc())) {
			product.setDownpaymentPercentage(new BigDecimal(mpd.getDownpayment_perc()));
		}
		product.setPricing(getPricingDetails(mpd));
		return product;
	}

	private PricingDetails getPricingDetails(MerchantProductDetails mpd) {
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails.setPlanId(mpd.getPricing());
		pricingDetails.setPlanTenureDetails(getPlanTenureDetailList(mpd.getPricing()));
		return pricingDetails;
	}

	private List<PlantTenureDetails> getPlanTenureDetailList(String pricing) {
		Plan plan = merchantAggregatorService.getPlanSummary(pricing);
		List<PlantTenureDetails> list = new ArrayList<PlantTenureDetails>();
		PlansTenureMap ptm3 = merchantAggregatorService.getPlanTenureDetails(plan.getSubPlanId3months());
		PlansTenureMap ptm6 = merchantAggregatorService.getPlanTenureDetails(plan.getSubPlanId6months());
		PlansTenureMap ptm9 = merchantAggregatorService.getPlanTenureDetails(plan.getSubPlanId9months());
		PlansTenureMap ptm12 = merchantAggregatorService.getPlanTenureDetails(plan.getSubPlanId12months());
		addToList(list, ptm3);
		addToList(list, ptm6);
		addToList(list, ptm9);
		addToList(list, ptm12);
		return list;
	}

	private void addToList(List<PlantTenureDetails> list, PlansTenureMap planTenureDetails) {
		list.add(getPlanTenureDetails(planTenureDetails));
	}

	private PlantTenureDetails getPlanTenureDetails(PlansTenureMap planTenurehMap) {
		PlantTenureDetails ptd = new PlantTenureDetails();
		ptd.setPlanTenureId(planTenurehMap.getPlanTenureId());
		ptd.setDownPaymentType(planTenurehMap.getDownpaymentType());
		ptd.setTenure(planTenurehMap.getTenure());
		if (isValidNumeric(planTenurehMap.getMerchantSubventionPercentage())) {
			ptd.setMerchantSubventionPercentage(new BigDecimal(planTenurehMap.getMerchantSubventionPercentage()));
		}
		if (isValidNumeric(planTenurehMap.getMerchantFeePercentage())) {
			ptd.setMerchantFeePercentage(new BigDecimal(planTenurehMap.getMerchantFeePercentage()));
		}
		if (isValidNumeric(planTenurehMap.getCustomerProcFeePercentage())) {
			ptd.setCustomerProcFeePercentage(new BigDecimal(planTenurehMap.getCustomerProcFeePercentage()));
		}
		if (isValidNumeric(planTenurehMap.getCustomerRoi())) {
			ptd.setCustomerRoi(new BigDecimal(planTenurehMap.getCustomerRoi()));
		}
		return ptd;
	}

	private boolean isValidNumeric(String val) {
		return (StringUtils.hasText(val) && !val.equalsIgnoreCase(NA));
	}

	private List<MerchantProductDetails> validateAndGetProducts(MerchantPricingRequest in,
			List<MerchantProductDetails> mpdList) {
		List<MerchantProductDetails> productList = null;
		if (StringUtils.hasText(in.getProductId())) {
			productList = new ArrayList<>();
			for (MerchantProductDetails mpd : mpdList) {
				if (EqualsHelper.equalsIgnoreCase(mpd.getProductId(), in.getProductId())) {
					productList.add(mpd);
				}
			}
		} else {
			productList = mpdList;
		}

		if (CollectionUtils.isEmpty(productList)) {
			throw new ValidationException(String.format("Invalid productId %s", in.getProductId()));
		}
		return productList;
	}

	private List<MerchantProductDetails> validateAndGetMerchantProductDetails(MerchantPricingRequest in) {
		if (in.getMerchantName() == null) {
			throw new ValidationException("merachantId cant be null");
		}
		List<MerchantProductDetails> mpdList = merchantAggregatorService
				.getMerchantProductDetails(in.getMerchantName());
		if (CollectionUtils.isEmpty(mpdList)) {
			throw new ValidationException(String.format("Invalid Merchant %s", in.getMerchantName()));
		}
		return mpdList;
	}

}
