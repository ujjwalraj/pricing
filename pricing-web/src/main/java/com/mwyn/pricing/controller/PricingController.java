package com.mwyn.pricing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.moneytap.pricing.api.PricingDetailsApi;
import com.moneytap.pricing.io.MerchantPricingRequest;

@RestController
@RequestMapping(value = "/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
public class PricingController {
	@Autowired private PricingDetailsApi pricingDetailsApi;

	@PostMapping(value = "/fetch_plans", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAvailablePlans(@RequestBody MerchantPricingRequest req) {
		return ResponseEntity.ok(pricingDetailsApi.executeApi(req));
	}

}
