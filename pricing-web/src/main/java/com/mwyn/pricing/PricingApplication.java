package com.mwyn.pricing;

import java.io.File;

import org.eclipse.jetty.server.AsyncNCSARequestLog;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.jetty.JettyServerCustomizer;
import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.moneytap", "com.mwyn"})
public class PricingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricingApplication.class, args);
	}
	
	@Bean
	public JettyServletWebServerFactory jettyEmbeddedServletContainerFactory(
			@Value("${server.port:10001}") final int port,
			@Value("${mt.server.threadPool.maxThreads:64}") final int maxThreads,
			@Value("${mt.server.threadPool.minThreads:64}") final int minThreads,
			@Value("${mt.server.threadPool.idleTimeout:5000}") final int idleTimeout,
			@Value("${mt.server.accesslog.dir:/opt/apps/moneytap/logs/pricing/}") final String logDir,
			@Value("${mt.server.accesslog.enabled:true}") final boolean accessLogEnabled) {

		final JettyServletWebServerFactory factory =  new JettyServletWebServerFactory(port);
		factory.addServerCustomizers(new JettyServerCustomizer() {
			@Override
			public void customize(final Server server) {
				if(accessLogEnabled){
					HandlerCollection handlers = new HandlerCollection();
					for (Handler handler : server.getHandlers()) {
						handlers.addHandler(handler);
					}

					handlers.addHandler(createRequestLogHandler(logDir));
					server.setHandler(handlers);
				}

				// Tweak the connection pool used by Jetty to handle incoming HTTP connections
				final QueuedThreadPool threadPool = server.getBean(QueuedThreadPool.class);
				threadPool.setMaxThreads(maxThreads);
				threadPool.setMinThreads(minThreads);
				threadPool.setIdleTimeout(idleTimeout);
			}
		});
		return factory;
	}

	private RequestLogHandler createRequestLogHandler(String logDir){
		AsyncNCSARequestLog log = new AsyncNCSARequestLog();
		log.setFilename(logDir + File.separator + "yyyy_mm_dd.request.log");
		log.setFilenameDateFormat("yyyy_MM_dd");
		log.setExtended(true);
		log.setLogServer(true);
		log.setLogLatency(true);
		log.setPreferProxiedForAddress(true);
		log.setRetainDays(90);
		log.setAppend(true);
		log.setLogCookies(false);
		log.setLogTimeZone("UTC");
		RequestLogHandler requestLogHandler = new RequestLogHandler();
		requestLogHandler.setRequestLog(log);
		return requestLogHandler;
	}

}
